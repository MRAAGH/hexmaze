![](https://files.mazie.rocks/Screenshot_2021-09-21_04-18-57.png)

![](https://files.mazie.rocks/Screenshot_2021-09-21_04-21-59.png)

Generates a maze on a hexagonal grid. The pourpose of this was to learn Rust.

For generating the maze the recursive backtracker algorithm is used. For placing the entrance and exit, Dijkstra's algorithm is used.

## Usage

```
$ git clone https://gitlab.com/MRAAGH/hexmaze.git
$ cd hexmaze
$ cargo run
```

`cargo run -- [HEIGHT] [WIDTH] [options]`


- `-s SEED` seed for generator (numeric)
- `-b ATTEMPTS` braided maze (instead of perfect maze)
- `-e` add entrance and exit
- `-w` print wide hexagons
- `-h` for help message

