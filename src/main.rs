extern crate getopts;
use getopts::Options;
use std::env;

extern crate rand;
use rand::{Rng, SeedableRng, rngs::StdRng};

mod map;
use map::Map;
use map::Pos;
use map::Cell;

fn print_help(program: &str, opts: Options) {
    let brief = format!("Usage: {} [HEIGHT] [WIDTH] [options]", program);
    print!("{}", opts.usage(&brief));
}

fn main() {

    // TODO: make these not mut
    let mut height:    usize = 10;
    let mut width:     usize = 25;
    let mut seed:      u64   = 0;
    let mut yes_exit:  bool  = false;
    let mut yes_braid: bool  = false;
    let mut num_braid: usize = 2;
    let mut yes_wide:  bool  = false;

    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optopt("s", "seed", "seed for generator (numeric)", "SEED");
    opts.optopt("b", "braid", "braided maze", "ATTEMPTS");
    opts.optflag("e", "exit", "add entrance and exit");
    opts.optflag("w", "wide", "print wide hexagons");
    opts.optflag("h", "help", "print this help message");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m }
        Err(f) => { panic!(f.to_string()) }
    };

    if matches.opt_present("h") {
        print_help(&program, opts);
        return;
    }

    if matches.opt_present("e") {
        yes_exit = true;
    }

    if matches.opt_present("w") {
        yes_wide = true;
    }

    if matches.opt_present("s") {
        //TODO: call print_help at any error
        let seed_str: String = matches.opt_str("s").unwrap();
        seed = seed_str.to_string().parse::<u64>().unwrap();
    }

    if matches.opt_present("b") {
        yes_braid = true;
        let braid_str: String = matches.opt_str("b").unwrap();
        num_braid = braid_str.to_string().parse::<usize>().unwrap();
    }

    let num_unnamed = matches.free.len();

    if num_unnamed == 0 {
        // no unnamed arguments, keep default dimensions.
    } else if num_unnamed == 1 {
        // 1 unnamed argument, make square dimensions
        height = matches.free[0].parse::<usize>().unwrap();
        width  = height;
    } else if num_unnamed == 2 {
        // 2 unnamed arguments, height and width
        height = matches.free[0].parse::<usize>().unwrap();
        width  = matches.free[1].parse::<usize>().unwrap();
    } else {
        // >2 unnamed arguments, error
        print_help(&program, opts);
        return;
    }



    let mut map = Map {width, height, cells:vec![]};

    map.make();

    recursive_backtrack(&mut map, seed);

    if yes_braid {
        random_braiding(&mut map, seed, num_braid);
    }

    if yes_exit {
        add_entrance_exit(&mut map);
    }

    map.print(yes_wide);

}



//TODO: move generator to different file

fn recursive_backtrack(map: &mut Map, seed: u64) {

    let mut rng = StdRng::seed_from_u64(seed);

    let mut stack: Vec<Pos> = vec![Pos{x:0,y:0}];

    while stack.len() > 0 {

        let cur_pos: Pos = *stack.last().unwrap();
        // mark visited
        map.at(cur_pos).visited = true;
        let neighbors = map.at(cur_pos).neighbors;

        let available: Vec<bool> = neighbors.iter().map(|pos| {
            if pos.is_none() {return false;}
            return !map.at(pos.unwrap()).visited;
        }).collect();

        let num_available: usize = available.iter().filter(|&a|*a).count();

        if num_available > 0 {
            // there are some available directions to generate in

            let cur_cell: &mut Cell = map.at(cur_pos);

            // choose a direction
            let mut chosen_index: usize =
                rng.gen_range(0..(num_available as u16)) as usize;

            // find nth true value in the vector available
            let mut chosen_dir: usize = 0;
            loop {
                if available[chosen_dir] { // found another true
                    if chosen_index == 0 {break;} // it was nth. end
                    chosen_index -= 1; // fewer steps remaining
                }
                chosen_dir += 1;
            }

            // remove wall on this side
            cur_cell.walls[chosen_dir] = false;

            // and also on other side
            let other_pos = cur_cell.neighbors[chosen_dir].unwrap();
            let other_cell: &mut Cell = map.at(other_pos);
            other_cell.walls[(chosen_dir+3)%6] = false;

            // move
            stack.push(other_pos);

        } else {
            // there are no available directions to generate.
            // backtrack a step
            stack.pop();
        }

    }

}


fn random_braiding(map: &mut Map, seed: u64, num_attempts: usize) {

    let mut rng = StdRng::seed_from_u64(seed);

    for _i in 0..num_attempts {

        // randomly select a cell and direction
        let x: usize = rng.gen_range(0..(map.width  as u16)) as usize;
        let y: usize = rng.gen_range(0..(map.height as u16)) as usize;
        let d: usize = rng.gen_range(0..6) as usize;
        let cell_here: &mut Cell = map.at( Pos{ x, y } );

        // skip this attempt if there's no neighbor to make a connection with
        let other_pos: Option<Pos> = cell_here.neighbors[d];
        if other_pos.is_none() {
            break;
        }

        // connect with neighbor
        cell_here.walls[d] = false;
        map.at(other_pos.unwrap()).walls[(d+3)%6] = false;

    }

}


fn add_entrance_exit(map: &mut Map) {
    let entrance: Pos = pick_entrance_or_exit(map, Pos{x:0,y:0});
    let exit: Pos = pick_entrance_or_exit(map, entrance);
    create_opening(map.at(entrance));
    create_opening(map.at(exit));
}


fn create_opening(cell: &mut Cell){
    // iterate walls
    for i in 0..cell.walls.len() {
        // is it a wall with the outside?
        if cell.neighbors[i].is_none() {
            // create opening
            cell.walls[i] = false;
            // done (one opening was enough)
            return;
        }
    }
}


fn pick_entrance_or_exit(map: &mut Map, start: Pos) -> Pos {

    // find distances from arbitrary point:
    let distances1 = bfs_distances(map, start);

    // find largest by edge:
    let mut max_dist: usize = 0;
    let mut max_pos: Pos = Pos { x: 0, y: 0 };
    for y in 0..map.height {
        for x in 0..map.width {
            // is it by the edge?
            let pos = Pos { x, y };
            let cell: &Cell = map.at(pos);
            if cell.neighbors.iter().any(|p| p.is_none()) {
                let dist = distances1[y][x];
                if dist > max_dist {
                    max_dist = dist;
                    max_pos = pos;
                }
            }
        }
    }

    max_pos

}

fn bfs_distances(map: &mut Map, start: Pos) -> Vec<Vec<usize>> {

    map.clear_visited();

    let mut frontier: Vec<Pos> = vec![start];

    let mut distances: Vec<Vec<usize>> =
        vec![ vec![ 0; map.width ]; map.height ];

    let mut cur_dist = 0;

    while frontier.len() > 0 {

        let mut new_frontier: Vec<Pos> = vec![];

        for pos in frontier {
            distances[pos.y][pos.x] = cur_dist;
            // mark visited
            map.at(pos).visited = true;
            let cell: Cell = *map.at(pos);
            for i in 0..cell.walls.len() {
                if !cell.walls[i] {
                    let other_pos: Pos = cell.neighbors[i].unwrap();
                    let other_cell: &mut Cell = map.at(other_pos);
                    if !other_cell.visited {
                        new_frontier.push(other_pos);
                    }
                }
            }

        }

        frontier = new_frontier;
        cur_dist += 1;
    }

    distances

}
