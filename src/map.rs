#[derive(Clone)]
#[derive(Copy)]
#[derive(Debug)]
pub struct Pos {
    pub x: usize,
    pub y: usize,
}

//TODO: add links: Vec<Pos> to Cell
//which keeps track of connected cells

#[derive(Clone)]
#[derive(Copy)]
#[derive(Debug)]
pub struct Cell {
    pub walls: [bool; 6],
    pub neighbors: [Option<Pos>; 6],
    pub visited: bool,
}

pub struct Map {
    pub width: usize,
    pub height: usize,
    pub cells: Vec<Vec<Cell>>,
}

impl Map {

    pub fn make(&mut self) {

        let default_cell = Cell {
            walls: [true; 6],
            neighbors: [None; 6],
            visited: false,
        };

        self.cells = vec![ vec![ default_cell; self.width ]; self.height ];

        for uy in 0..self.height {
            for ux in 0..self.width {
                let x = ux as isize;
                let y = uy as isize;
                if x%2==0 {
                    /*     1
                     *     |
                     *     _           _
                     *  2-| |-0   =>  / \
                     *    \_/         \_/
                     *   / | \
                     *  3  4  5
                     */
                    self.cells[uy][ux].neighbors[0] = self.make_link(x+1, y+0);
                    self.cells[uy][ux].neighbors[1] = self.make_link(x+0, y+1);
                    self.cells[uy][ux].neighbors[2] = self.make_link(x-1, y+0);
                    self.cells[uy][ux].neighbors[3] = self.make_link(x-1, y-1);
                    self.cells[uy][ux].neighbors[4] = self.make_link(x+0, y-1);
                    self.cells[uy][ux].neighbors[5] = self.make_link(x+1, y-1);
                } else {
                    /*
                     *     1
                     *  2  |  0
                     *   \ _ /         _
                     *    / \     =>  / \
                     *  3-|_|-5       \_/
                     *     |
                     *     4
                     */
                    self.cells[uy][ux].neighbors[0] = self.make_link(x+1, y+1);
                    self.cells[uy][ux].neighbors[1] = self.make_link(x+0, y+1);
                    self.cells[uy][ux].neighbors[2] = self.make_link(x-1, y+1);
                    self.cells[uy][ux].neighbors[3] = self.make_link(x-1, y+0);
                    self.cells[uy][ux].neighbors[4] = self.make_link(x+0, y-1);
                    self.cells[uy][ux].neighbors[5] = self.make_link(x+1, y+0);
                }
            }
        }

    }

    pub fn at(&mut self, pos: Pos) -> &mut Cell {
        &mut self.cells[pos.y][pos.x]
    }

    fn make_link(&self, x: isize, y: isize) -> Option<Pos> {
        if self.check_within(x, y) {
            return Some( Pos { x: x as usize, y: y as usize } );
        }
        None
    }

    fn check_within(&self, x:isize, y:isize) -> bool {
        x >= 0 && y >= 0
            && x < self.width as isize
            && y <self.height as isize
    }

    pub fn clear_visited(&mut self) {
        for row in &mut self.cells {
            for mut cell in row {
                cell.visited = false;
            }
        }
    }

    pub fn print(&self, wide_hexagons: bool) {

        let sp = if wide_hexagons {"  "} else {" "};
        let us = if wide_hexagons {"__"} else {"_"};

        // very first line has only spaces and underscores
        for cell_pair in (&(self.cells.last().unwrap())).chunks(2) {
            if cell_pair.len() > 1 {
                print!(" {} {}",
                       sp,
                       // potentially entrance at the very top:
                       if cell_pair[1].walls[1] {us} else {sp},
                   );
            }
        }
        println!(""); // end of first line
        print!(" "); // second line always begins with space
        for cell_pair in (&(self.cells.last().unwrap())).chunks(2) {
            if cell_pair.len() > 1 {
                //      "_/ \"
                print!("{}{}{}{}",
                       // potentially entrance at the top:
                       if cell_pair[0].walls[1] {us} else {sp},
                       // potentially entrance on the left of top cell:
                       if cell_pair[1].walls[2] {"/"} else {" "},
                       sp,
                       // potentially entrance on the right of top cell:
                       if cell_pair[1].walls[0] {"\\"} else {" "},
                       );
            }
        }
        // even width maze has additional underscore in second line
        if self.width%2 == 1 {
            let last_cell = self.cells.last().unwrap().last().unwrap();
            print!("{}",
                   // unless it is an entrance
                   if last_cell.walls[1] {us} else {sp},
                   );
        }

        let mut is_first_loop = true;

        // iterate rows to print all lines after first two:
        for row in self.cells.iter().rev() {

            if is_first_loop {
                is_first_loop = false;
            } else {
                // even width maze has additional backslash
                // at the end of previous line.
                // except in first loop.
                // This is here because we need information about
                // this maze row to finish the end of previous line.
                // It would be more logical to have this at the
                // end of the loop but this information is not
                // available there.
                if self.width%2 == 0 {
                    let last_cell = &row.last().unwrap();
                    print!("{}",
                           // unless it is an entrance
                           if last_cell.walls[0] {"\\"} else {" "});
                }
            }
            // now is actually the end of previous row
            println!("");

            // first line in this maze row:
            for cell_pair in row.chunks(2) {
                //      "/ \_"
                print!("{}{}{}{}",
                       if cell_pair[0].walls[2] {"/"} else {" "},
                       sp,
                       if cell_pair[0].walls[0] {"\\"} else {" "},
                       if cell_pair.len() > 1
                       && cell_pair[1].walls[4] {us} else {sp},
                       );
            }
            if self.width%2 == 0 {
                let last_cell = &row.last().unwrap();
                print!("{}",
                       if last_cell.walls[5] {"/"} else {" "});
            }
            // end of first line in this maze row
            println!("");

            // second line in this maze row:
            for cell_pair in row.chunks(2) {
                //      "\_/ "
                print!("{}{}{}{}",
                       if cell_pair[0].walls[3] {"\\"} else {" "},
                       if cell_pair[0].walls[4] {us} else {sp},
                       if cell_pair[0].walls[5] {"/"} else {" "},
                       sp,
                       );
            }

        }

        println!("");

    }

}
